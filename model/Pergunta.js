const Sequelize =  require('sequelize')
const dbConnect = require('../database/database')

const Pergunta = dbConnect.define('pergunta',{
    titulo: {
        type: Sequelize.STRING,
        allowNull: false
    },

    descricao:{
        type: Sequelize.TEXT,
        allowNull:false
    }
})

Pergunta.sync( {force: false} )

module.exports = Pergunta