const Sequelize =  require('sequelize')
const dbConnect = require('../database/database')

const Resposta = dbConnect.define('respostas',{
    corpo:{
        type: Sequelize.TEXT,
        allowNull:false
    },
    perguntaId:{
        type: Sequelize.INTEGER,
        allowNull: false
    }
})

Resposta.sync({force:false })

module.exports =  Resposta