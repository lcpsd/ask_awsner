const express = require('express')
const app = express()
const bodyparser = require("body-parser")
const dbConnect = require('./database/database')
const modelPergunta =  require('./model/Pergunta')
const modelResposta =  require('./model/Resposta')

//database
    dbConnect.authenticate()
    .then(() => {
        console.log('db OK')
    }).catch( (erro) => {
        console.log(erro)
    } )

//rotas
    app.set('view engine', 'ejs')
    app.use(express.static('public'))
    app.use(bodyparser.urlencoded({extended: true}))
    app.use(bodyparser.json())

    //rota raiz
    app.get('/', (req, res) =>{
        var deletedInfo = false
            modelPergunta.findAll({raw: true, order:[
                ['id','DESC']
            ]}).then(perguntas => {

                res.render('index',{
                    perguntas: perguntas,
                    deletedInfo: deletedInfo
                })
        })
        
    })

    //Rota para realizar pergunta
    app.get('/perguntar', (req, res) => {
        res.render('perguntar')
    })

    //rota para salvar pergunta no banco de dados
    app.post('/salvarpergunta', (req, res) =>{
        var {titulo} = req.body
        var {descricao} = req.body
        
        modelPergunta.create({
            titulo: titulo,
            descricao:descricao
        }).then( () => res.redirect("/"))
    })

    //rota para resgatar pergunta pelo id
    app.get('/pergunta/:id', (req, res) =>{
        var {id} =  req.params
        
        modelPergunta.findOne({
            where:{ id: id}

        }).then( pergunta => {
            if(pergunta != undefined){

                modelResposta.findAll({
                    where: {perguntaId: pergunta.id}
                }).then((respostas => {
                    res.render("paginaPergunta",{
                        pergunta: pergunta,
                        respostas: respostas
                    })
                }))
            }
            else{
                res.redirect("/")
            }
        })
    })
    
    //rota para responder a pergunta
    app.post('/responder', (req,res) => {
        var {corpoResposta} = req.body
        var {perguntaId} = req.body
        modelResposta.create({
            corpo: corpoResposta,
            perguntaId: perguntaId
        }).then( () => {
            res.redirect("/pergunta/"+ perguntaId)
        })
    })

    //rota para deletar a pergunta 
    app.post('/deletarPergunta', (req, res) =>{
        var {perguntaId} = req.body
        var {tituloPergunta} = req.body
        var deletedInfo = true

        // apagar todos os registros de perguntas com este id
        modelPergunta.destroy({ 
            where: {id: perguntaId}
        }).then(() => { 
            // apagar todos os registros de respostas que tenham o id da pergunta
            modelResposta.destroy({
                where:{perguntaId:perguntaId}
            }).then( () => {
                modelPergunta.findAll({raw: true, order:[
                    ['id','DESC']
                ]}).then((perguntas) => {
                    res.render('index',{
                        tituloPergunta:tituloPergunta,
                        deletedInfo: deletedInfo,
                        perguntas: perguntas
                    })
                })  
            })  
        })
    })

app.listen(3333, () => console.log('ok'))

